---
marp: true
---

# Лекция 3. Классы

Фёдор Короткий

---

# Структуры

```c++
struct User {
    std::string name;
    int age;
};

User user; // создание экземпляра структуры
user.name = "Fedor";

User* p;
p->name = "Fedor";
```

 - Объявление структуры создаёт новый тип
 - Структура состоит из набора полей
 - После `}` обязательно идёт `;`
 - Обращение к полям идёт через `.` или `->`
 
---

# Методы

```c++
struct User {
    int age;
    
    bool IsOldEnough()
    {
        return age > 18;
    }
};

User user;
if (!user.IsOldEnough()) {
    return; 
}
```

 * Функция внутри тела структуры называется методом
 * Метод имеет доступ к полям структуры
 * Для вызова метода используется `.` или `->`

---

# Переменная `this`

```c++
struct User {
    int age;

    bool IsOldEnough()
    {
        // Неявный аргумент:
        // User* this;
        
        if (age > 18) { // ...
        
        if (this->age > 18) { // неявно происходит
    }
};

User u;

if (u.IsOldEnough()) { // ...

if (User::IsOldEnough(&u)) { // неявно происходит
```

---

# Константность

```c++
bool CheckUser(const User& user)
{
    if (!user.IsOldEnough()) { // compilation error
        return false;
    }
    if (user.Name == "Fedor") {
        return false;
    }
    return true;
}
```

 * Методы могут быть помечены ключевым словом `const`
 * Нельзя вызывать __не const__ методы на константной ссылке

---

# Константность

```c++
struct User {
    bool IsOldEnough() const;
    
    void UpdatePassword(std::string password);
};
```

 * `const` - метод "логически" не меняет состояние структуры
 * Другой смысл `const` относится к многопоточности. В этом курсе про него мы говорить не будем.

---

# Константность

```c++
struct User {
    int age;

    bool IsOldEnough() const {
        age += 1; // compilation error
    }
};
```

---

# Конструкторы

```c++
struct User {
    std::string username;
    int age;

    User(std::string name, int _age)
        : username(name)
    {
        age = _age;    
    }
};

User u("Fedor", -1);
```

 * Конструктор - особый метод структуры
 * Конcтруктор должен инициализировать поля класса

---

# Конструкторы

```c++
struct User {
    std::string username;
    int age;

    User(std::string name, int _age)
        : username(name)
        , age(_age)
    {
        age += 1;
    }
};

User u("Fedor", -1);
```

 * Имя конструктора такое же, как имя структуры
 * У конструктора нет возвращаемого значения
 * Перед телом конструктора идёт _список инициализации_
 * В теле конструктора доступны поля, так же как и в методе

---

# const поля

```c++
struct User {
    const std::string username;

    User(std::string name)
    {
        username = name; // compilation error
    }
};
```

---

# const поля

```c++
struct User {
    const std::string username;

    User(std::string name)
        : username(name)
    { }
};
```

 * `const` поля можно инициализировать только в _списке инициализации_

---

# default конструктор

```c++
struct User {
    std::string username;
};

User u{};
```
 * Есть ли конструктор у типа `User`?

---

# default конструктор

```c++
struct User {
    std::string username;
    
    // неявно дописан компилятором
    User()
        : username()
    { }
};
```

 * Есть ли конструктор у типа `User`?

---

# default конструктор

 * Если в классе нет других конструкторов, то неявно создаётся `default`-конструктор
 * Если определён пользовательский конструктор, то `default` конструктор не создаётся
 * Объект нельзя создать, не позвав конструктор

```
struct Rect {
    // пользователь обязан указать 2 точки
    Rect(Point topLeft, Point bottomRight);
};

Rect r; // compilation error
```

---

# Список инциализации

```
struct Window {
    Rect r;
    
    Window()
        : r({0, 0}, {800, 600})
    { }
};
```

 * Список инициализации нужен, чтобы сконструировать поля **без** `default` конструктора

---

# Классы, Модификаторы доступа

```
class Vector {
public:
    void push_back(int x);

private:
    int* begin_;
};

Vector v;
v.begin_ = nullptr; // compilation error
```

 - Классы объявляются с помощью ключевого слова `class`
 - Классы отличаются от структур тем, что в них пишут _модификаторы доступа_
 - Метод может быть `public` или `private`
 - Поле может быть `public` или `private`

---

# Модификаторы доступа

```c++
class Vector {
public:
    void push_back(int x);

private:
    int* begin_;
};
```

 - Не стоит делать public поля
