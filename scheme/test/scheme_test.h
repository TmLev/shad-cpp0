#pragma once

#include <catch.hpp>

struct SchemeTest {
    SchemeTest() {
        // Create new interpreter HERE.
    }

    // Implement following methods.
    void ExpectEq(std::string expression, std::string result) {
        // Silence unused variable warning. Remove this code.
        (void)expression;
        (void)result;

        throw std::runtime_error("ExpectEq is not implemented");
    }

    void ExpectNoError(std::string expression) {
        (void)expression;

        throw std::runtime_error("ExpectNoError is not implemented");
    }

    void ExpectSyntaxError(std::string expression) {
        (void)expression;

        throw std::runtime_error("ExpectSyntaxError is not implemented");
    }

    void ExpectRuntimeError(std::string expression) {
        (void)expression;

        throw std::runtime_error("ExpectRuntimeError is not implemented");
    }

    void ExpectNameError(std::string expression) {
        (void)expression;

        throw std::runtime_error("ExpectNameError is not implemented");
    }
};
